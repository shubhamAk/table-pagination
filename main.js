import React from 'react'
import ReactDOM from 'react-dom'
import ParentComp from 'parent';

ReactDOM.render(<ParentComp />, document.getElementById('app'));