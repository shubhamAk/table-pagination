var path = require("path");

module.exports = {
    entry: './main.js',
    output: {
      filename: 'bundle.js',
    },
    resolve: {
      modules: [
        path.resolve('./src'),
        path.resolve('./node_modules')
      ],
      extensions: ['.js','.jsx']
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015', 'react']
          }
        },
      ]
    },
    devServer: {
      inline: true,
      port: 3000
   }

}