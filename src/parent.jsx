import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

import TableData from 'component/table-data';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton   from 'material-ui/IconButton';
import SubmitIcon   from 'material-ui/svg-icons/content/send';

import DisplayTableParent from 'component/display-table-parent';
import PageNavigation from 'component/page-navigation';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();


let Example = React.createClass({
	
	getInitialState() {
		return {
			currentPage: 1,
			tableData: TableData,
			totalPage: TableData.length,
			maxRowToDisplay: 10, 
		}
	},
  
  
  updateMember(member) {
  	this.setState({tableData: member});
  },
  

  updatePage(page) {
  	this.setState({currentPage: page});
  },

  
	render() {
		return (
      <MuiThemeProvider>
  			<div>
  				<PageNavigation 
    				totalPage={this.state.totalPage}
    				currentPage={this.state.currentPage}
    				maxRowToDisplay={this.state.maxRowToDisplay}
    				onPageChange={this.updatePage}/>
  				<DisplayTableParent 
  					tableData={this.state.tableData}
  					pageSize={this.state.currentPage}
  					maxRowToDisplay={this.state.maxRowToDisplay}
  					updateTableData={this.updateMember}/>
    			
  			</div>
      </MuiThemeProvider>  
		)
	}
});

export default Example;