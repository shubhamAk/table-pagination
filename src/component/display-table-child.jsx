import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import SubmitIcon   from 'material-ui/svg-icons/content/send';
import RaisedButton from 'material-ui/RaisedButton';


let DisplayTableChild = React.createClass({
	
	propTypes: {
		id: React.PropTypes.number,
		name: React.PropTypes.string,
		status: React.PropTypes.string,
		performAction: React.PropTypes.func,
	},


	render() {
		let {id, name, status} = this.props;
		return(
			<TableRow>
	      <TableRowColumn>{id}</TableRowColumn>
	      <TableRowColumn>{name}</TableRowColumn>
	      <TableRowColumn>{status}</TableRowColumn>
	      <TableRowColumn>
		      <RaisedButton
	          label='Delete'
	          primary={true}
	          labelPosition='before'
	          icon={<SubmitIcon />}
	          onTouchTap={()=>{this.props.performAction(id)}}/>
        </TableRowColumn> 
		  </TableRow> 
		)	
	}

});

export default DisplayTableChild;		