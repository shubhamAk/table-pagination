import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import _ from 'underscore';
import DisplayTableChild from './display-table-child';

import RaisedButton from 'material-ui/RaisedButton';



let DisplayTableParent = React.createClass({
  
  propTypes: {
  	maxRowToDisplay: React.PropTypes.number,
  	pageSize: React.PropTypes.number,
    tableData: React.PropTypes.arrayOf(React.PropTypes.object),
    updateTableData: React.PropTypes.func,
  },


  performAction(id) {

    let members = _.filter(this.props.tableData,(member)=> {
      if (member.id !== id) return member;
    })
    
    this.props.updateTableData(members);
    
  },

  
  displayTableData() {
  	let start = 0;
  	if (this.props.pageSize !== 1) {
  		start = (this.props.pageSize - 1) * this.props.maxRowToDisplay
  	}
  	let tableData = this.props.tableData.slice(start,this.props.pageSize * this.props.maxRowToDisplay)
    	return tableData.map((data,index) => {
	  		  		return (
	  		  			<DisplayTableChild 
	  							key={data.id} 
	  							id= {data.id} 
	  							name={data.name} 
	  							status={data.status}
	  							performAction={this.performAction}/>)
  	});
  },


	render() {
		return (
			<Table>
				<TableHeader displaySelectAll={false} adjustForCheckbox={false}>
	  			<TableRow>
		        <TableHeaderColumn>ID</TableHeaderColumn>
		        <TableHeaderColumn>Name</TableHeaderColumn>
		        <TableHeaderColumn>Status</TableHeaderColumn>
		        <TableHeaderColumn>PerformAction</TableHeaderColumn>
	  			</TableRow>
	    	</TableHeader>
			    <TableBody displayRowCheckbox={false}>
			      {this.displayTableData()}
	  			</TableBody>
			</Table>
  	)
	}
});

export default DisplayTableParent;
