import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';


let PageNavigation = React.createClass({
	
	propTypes: {
		totalPage: React.PropTypes.number,
		onPageChange: React.PropTypes.func,
		maxRowToDisplay: React.PropTypes.number,
	},


  getInitialState() {
  	return {
  		value: 1,
  	}
  },
  

	handleChange(event,index,value) {
		this.setState({value: value});
		this.props.onPageChange(value);
	},

  
  displayDropDownList() {
  	let page = [];
  	const pageSize = Math.ceil(this.props.totalPage / this.props.maxRowToDisplay)
  	for(let index = 1 ; index <= pageSize; index++) {
  		page.push(<MenuItem value={index} key={`menuItem ${index}`} primaryText={`page ${index}`} />);	
  	}
  	return page;
  },


	render() {
		return (
			<div style={{display: 'flex',flex: 1,alignItems: 'center',justifyContent: 'center'}}>
				<DropDownMenu value={this.state.value} onChange={this.handleChange}>
          {this.displayDropDownList()}
        </DropDownMenu>
			</div>
		)
  }
});

export default PageNavigation;  		